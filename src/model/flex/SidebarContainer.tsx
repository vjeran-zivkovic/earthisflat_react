import React from "react";
import { IItem, ICollection } from "../common/collection";
import SidebarItem from "./SidebarItem";
import { connect } from "react-redux";
import { itemSelectAction } from "../../store/items/items-actions";

interface IProps {
	collection: ICollection;
  onItemSelect: (item: IItem) => void;
  itemsToShow: number;
}

const SidebarContainer: React.FC<IProps> = (props) => (
  <div className="sidebar-container">
    {props.collection.items
      .slice(0, props.itemsToShow)
      .map((item: IItem) =>
        <SidebarItem
          key={item.data[0].nasa_id}
          item={item}
          onItemSelect={props.onItemSelect}
        />
      )
    }
  </div>
);


const mapDispatchToProps = (dispatch: any) => {
	return {
		onItemSelect: (item: IItem) => dispatch(itemSelectAction(item)),
	};
};

export default connect(null, mapDispatchToProps)(SidebarContainer);
