import React from "react";
import Content from "./Content";
import { IItem } from "../common/collection";

interface IProps {
  selectedItem: IItem | undefined;
}

const ContentContainer: React.FC<IProps> = (props) => (
  <div className="content-container">
    {props.selectedItem &&
      <Content {...props} />
    }
  </div>
);

export default ContentContainer;
