import React from "react";
import { IItem } from "../common/collection";

interface IProps {
  selectedItem: IItem | undefined;
}

class Content extends React.Component<IProps> {
  render() {
    const selectedItem = this.props.selectedItem!;
    return (
      <>
        <h3>{selectedItem.data[0].title}</h3>
        <img src={selectedItem.links.find(link => link.rel === "preview")?.href} alt={""} />
        <p>{selectedItem.data[0].description}</p>
      </>
    );
  }
}

export default Content;
