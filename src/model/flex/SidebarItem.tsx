import React from "react";
import { Link } from "react-router-dom";
import { IItem } from "../common/collection";

interface IProps {
  item: IItem;
  onItemSelect: (item: IItem) => void;
}

class SidebarItem extends React.Component<IProps> {
  render() {
    const { data, links } = this.props.item;
    return (
      <Link to={`${data[0].nasa_id}`} onClick={() => this.props.onItemSelect(this.props.item)}>
        <div className="sidebar-item">
          <h4>{data[0].title}</h4>
          <img src={links.find(link => link.rel === "preview")?.href} alt={""} />
        </div>
      </Link>
    )
  }
}

export default SidebarItem;
