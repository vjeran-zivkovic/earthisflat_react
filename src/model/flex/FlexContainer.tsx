import React from 'react';
import SidebarContainer from './SidebarContainer';
import ContentContainer from './ContentContainer';
import { connect } from 'react-redux';
import { IAppState } from '../../store/app-state';
import { ICollection, IItem } from '../common/collection';
import { FormattedMessage } from 'react-intl';
import ScrollToTop from '../common/ScrollToTop';

interface IProps {
	collection: ICollection | undefined;
  itemsToShow: number;
  selectedItem: IItem | undefined;
}

const FlexContainer: React.FC<IProps> = (props) => (
  <div className="flex-container">
    {props.collection && props.collection.metadata.total_hits > 0 ?
      <>
        <SidebarContainer
          collection={props.collection}
          itemsToShow={props.itemsToShow}
        />
        <ContentContainer
          selectedItem={props.selectedItem}
        />
        <ScrollToTop />
      </>
      :
      <FormattedMessage id={"flexbox.noResults"} />}
  </div>
);


const mapStateToProps = (state: IAppState) => {
	return {
    collection: state.items.collection,
    itemsToShow: state.items.itemsToShow,
    selectedItem: state.items.selectedItem,
	};
};

export default connect(mapStateToProps)(FlexContainer);
