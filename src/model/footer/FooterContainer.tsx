import React from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import Footer from "./Footer";
import { IAppState } from "../../store/app-state";
import { ICollection } from "../common/collection";
import { showMoreItemsAction } from "../../store/items/items-actions";

interface IProps {
  collection: ICollection | undefined;
  onShowMore: () => void;
  itemsToShow: number;
}

const FooterContainer: React.FC<IProps> = (props) => (
  <div className="footer-container">
    {props.collection && props.collection.metadata.total_hits > 0 &&
      <Footer {...props} />
    }
    <div className="text-center">
      <FormattedMessage id={"footer.copyrightMessage"} />
    </div>
  </div>
);


const mapStateToProps = (state: IAppState) => {
	return {
    collection: state.items.collection,
    itemsToShow: state.items.itemsToShow,
	};
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onShowMore: () => dispatch(showMoreItemsAction()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FooterContainer);
