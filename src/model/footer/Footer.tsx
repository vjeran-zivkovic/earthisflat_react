import React from "react";
import Button from "react-bootstrap/Button";
import { FormattedMessage } from "react-intl";
import { ICollection } from "../common/collection";

interface IProps {
  collection: ICollection | undefined;
  onShowMore: () => void;
  itemsToShow: number;
}

class Footer extends React.Component<IProps> {
  render() {
    const isAllItemsShown = this.props.collection!.metadata.total_hits <= this.props.itemsToShow;
    const messageId = isAllItemsShown ? "footer.nothingMoreToShow" : "footer.showMore";
    return (
      <Button onClick={this.props.onShowMore} disabled={isAllItemsShown}>
        <FormattedMessage id={messageId} />
      </Button>
    );
  }
}

export default Footer;
