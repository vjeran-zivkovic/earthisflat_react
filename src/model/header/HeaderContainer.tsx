import React from "react";
import { connect } from "react-redux";
import { changeLangAction } from "../../store/header/header-actions";
import { LangEnum } from "../../store/header/header-reducer";
import Header from "./Header";
import { fetchCollectionAction } from "../../store/items/items-actions";

interface IProps {
  onChangeLang: (lang: LangEnum) => void;
  onSearch: (str: string) => void;
}

class HeaderContainer extends React.Component<IProps> {
  render() {
    return (
      <div className="header-container">
        <Header {...this.props} />
      </div>
    );
  }
}


const mapDispatchToProps = (dispatch: any) => {
  return {
    onChangeLang: (lang: LangEnum) => dispatch(changeLangAction(lang)),
    onSearch: (str: string) => dispatch(fetchCollectionAction(str))
  };
}


export default connect(null, mapDispatchToProps)(HeaderContainer)
