import React from "react";
import { LangEnum } from "../../store/header/header-reducer";
import { FormattedMessage } from "react-intl";
import Button from 'react-bootstrap/Button';
import HeaderForm from "./HeaderForm";

interface IProps {
  onChangeLang: (lang: LangEnum) => void;
  onSearch: (str: string) => void;
}

class Header extends React.Component<IProps> {
  render() {
    return (
      <>
        <div className="grid-container">
          <div className="item1" />
          <div className="item2" />
          <div className="item3"><h1><FormattedMessage id={"header.flatEarth"} /></h1></div>
          <div className="item4">
            <Button onClick={() => this.props.onChangeLang(LangEnum.HR)}>{"HR"}</Button>
            <Button onClick={() => this.props.onChangeLang(LangEnum.EN)}>{"EN"}</Button>
          </div>
        </div>
        <div>
          <HeaderForm
            onSubmit={({ search }: any) => this.props.onSearch(search ?? '')}
          />
        </div>
      </>
    )
  }
}

export default Header;
