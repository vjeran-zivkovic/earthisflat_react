import React from 'react'
import { Field, reduxForm, InjectedFormProps } from 'redux-form'
import { FormattedMessage } from 'react-intl';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

const HeaderForm: React.FC<InjectedFormProps> = (props) => {
  const { handleSubmit } = props;
  return (
    <Form onSubmit={handleSubmit}>
      <Field
        name={'search'}
        component={renderField}
        type="text"
      />
      <Button type="submit"><FormattedMessage id={"header.search"} /></Button>
    </Form >
  )
}

const renderField = ({ input, meta }: any) => (
  <Form.Group>
    <Form.Control {...input} type="text" />
    {meta.touched && meta.error &&
      <Form.Text className="error">{meta.error}</Form.Text>
    }
  </Form.Group>
)

export default reduxForm({
  form: 'header'
  //@ts-ignore
})(HeaderForm)
