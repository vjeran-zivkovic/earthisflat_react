import React from "react";
import { Link } from "react-router-dom";
import { HOME } from "../../App";
import { FormattedMessage } from "react-intl";

interface IState {
  error: Error | undefined;
  info: any;
}

class ErrorBoundary extends React.Component<{}, IState> {
  state = {
    error: undefined,
    info: undefined
  };

  static getDerivedStateFromError(error: Error) {
    // Update state so the next render will show the fallback UI.
    return { error };
  }

  componentDidCatch(error: Error, errorInfo: any) {
    // Log the error to an error reporting service
  }

  render() {
    if (this.state.error) {
      return <Link to={HOME}><FormattedMessage id={"errorBoundary.errorMessage"} /></Link>;
    }

    return this.props.children; 
  }
}

export default ErrorBoundary;
