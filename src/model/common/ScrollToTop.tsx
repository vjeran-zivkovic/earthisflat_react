import React from 'react';
import Button from 'react-bootstrap/Button';

interface IState {
  style: React.CSSProperties,
}

class ScrollToTop extends React.Component<{}, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      style: {
        display: 'none',
        position: 'fixed',
        width: 'auto',
        height: 'auto',
        bottom: '30px',
        right: '30px',
        fontSize: 'xx-large',
        zIndex: 99,
      } as React.CSSProperties,
    }

    window.onscroll = () => {
      this.setState(({ style }) => ({
        style: {
          ...style,
          display: window.pageYOffset === 0 ? 'none' : 'block',
        }
      }));
    };
  }

  handleScroll() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <Button style={this.state.style} onClick={this.handleScroll}>
        &#708;
      </Button>
    );
  }
}

export default ScrollToTop;
