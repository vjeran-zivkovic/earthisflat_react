export interface ICollection {
  metadata: IMetadata
  items: IItem[]
}

interface IMetadata {
  total_hits: number
}

export interface IItem {
  data: IData[],
  links: ILink[]
}

interface IData {
  nasa_id: string,
  title: string,
  description: string,
  location: string
}

interface ILink {
  rel: string, // preview - jpg, captions - vtt
  href: string
}
