import React from 'react';
import './App.scss';
import HeaderContainer from './model/header/HeaderContainer';
import { Router, Route, Redirect } from 'react-router-dom';
import IntlGlobalProvider from './i18n/IntlGlobalProvider';
import history from './common/history'
import FooterContainer from './model/footer/FooterContainer';
import ErrorBoundary from './model/common/ErrorBoundary';
import FlexContainer from './model/flex/FlexContainer';

export const HOME = "/";
export const ITEMS = "/items/";

const App: React.FC = () => (
  <IntlGlobalProvider>
    <Router history={history}>
      <Route render={() =>
        <ErrorBoundary>
          <HeaderContainer />
        </ErrorBoundary>
      } />
      <Route path={`${ITEMS}:id?`} render={() =>
        <ErrorBoundary>
          <FlexContainer />
        </ErrorBoundary>
      } />
      <Route render={() =>
        <ErrorBoundary>
          <FooterContainer />
        </ErrorBoundary>
      } />
      {/* page reload */}
      <Route exact path="/*" render={() => <Redirect to={HOME} />} />
    </Router>
  </IntlGlobalProvider>
);

export default App;
