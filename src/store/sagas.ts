import { spawn } from "redux-saga/effects";
import watchItemsSaga from "./items/items-saga";

export default function* rootSaga() {
  yield spawn(watchItemsSaga);
}
