import { IAction } from "../action";
import * as actionTypes from "./header-action-types";
import { LangEnum } from "./header-reducer";

export const changeLangAction = (lang: LangEnum): IAction<LangEnum> =>
  ({ type: actionTypes.CHANGE_LANG, payload: lang });
