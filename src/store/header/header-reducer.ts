import { IAction } from "../action";
import { CHANGE_LANG } from "./header-action-types";

export enum LangEnum {
  EN = "en",
  HR = "hr"
}

export interface IHeaderState {
  locale: {
    lang: LangEnum
  }
}

const initialState: IHeaderState = {
  locale: {
    lang: LangEnum.EN
  }
}

export default function (state = initialState, action: IAction<LangEnum | string[]>): IHeaderState {
  switch (action.type) {
    case CHANGE_LANG:
      const changeLangPayload = action.payload as LangEnum;
      return { ...state, locale: { lang: changeLangPayload } }
    default:
      return state;
  }
}
