import { IHeaderState } from "./header/header-reducer";
import { IItemsState } from "./items/items-reducer";

export interface IAppState {
  header: IHeaderState;
  items: IItemsState;
}