import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { AppLocalStorage } from "../common/app-local-storage";
import { IAppState } from "./app-state";
import rootReducer from "./reducers";
import rootSaga from "./sagas";

const composeEnhancers = composeWithDevTools({
  // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

const persistedData = AppLocalStorage.loadState();
const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, persistedData, composeEnhancers(
  applyMiddleware(sagaMiddleware),
  // other store enhancers if any
));

store.subscribe(() => {
  const state = store.getState();
  const loadedLang = AppLocalStorage.loadState()?.locale?.lang;
  
  if (state.header.locale.lang !== loadedLang) {
    AppLocalStorage.saveState({ header: { locale: state.header.locale } } as IAppState);
  }
})

sagaMiddleware.run(rootSaga);

export default store;