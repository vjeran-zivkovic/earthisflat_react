import { IAction } from "../action";
import * as actionTypes from "./items-action-types";
import { ICollection, IItem } from "../../model/common/collection";

export interface IItemsState {
  collection: ICollection | undefined;
  selectedItem: IItem | undefined,
  itemsToShow: number
}

const defaultItemsCount = 5;

const initialState: IItemsState = {
  collection: undefined,
  selectedItem: undefined,
  itemsToShow: defaultItemsCount
}

export default function (state = initialState, action: IAction<ICollection | IItem>): IItemsState {
  switch (action.type) {
    case actionTypes.FETCH_ITEMS:
      return {
        ...state,
        selectedItem: undefined,
        itemsToShow: defaultItemsCount
      };
    case actionTypes.FETCH_ITEMS_SUCCESS:
      const fetchItemsPayload = action.payload as ICollection;
      return { ...state, collection: fetchItemsPayload };
    case actionTypes.ITEM_SELECT:
      return { ...state, selectedItem: action.payload as IItem };
    case actionTypes.SHOW_MORE_ITEMS:
      return { ...state, itemsToShow: state.itemsToShow + defaultItemsCount };
    default:
      return state;
  }
}
