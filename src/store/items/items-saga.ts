import { put, takeLatest } from 'redux-saga/effects';
import * as actionTypes from './items-action-types';
import * as service from './items-service';
import * as actions from './items-actions';
import { IAction } from '../action';
import history from '../../common/history';
import { ITEMS } from '../../App';
import { ICollection } from '../../model/common/collection';


function* fetchItemsSaga(action: IAction<string>) {
  try {
    const resp = yield service.fetchItems(action.payload as string);
    const collection = JSON.parse(resp.text).collection as ICollection;
    yield put(actions.fetchCollectionSuccessAction(collection));
    history.push(ITEMS);
  } catch (error) {
    yield put(actions.fetchCollectionErrorAction(error));
  }
}

export default function* watchItemsSaga() {
  yield takeLatest(actionTypes.FETCH_ITEMS, fetchItemsSaga);
}
