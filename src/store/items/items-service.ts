import superagent from "superagent";

export const fetchItems = (str: string) => {
  try {
    return superagent.get(`https://images-api.nasa.gov/search?keywords=${str}`);
  } catch (err) {
    throw err;
  }
};
