import { IAction } from "../action";
import * as actionTypes from "./items-action-types";
import { ICollection, IItem } from "../../model/common/collection";

export const fetchCollectionAction = (str: string): IAction<string> =>
  ({ type: actionTypes.FETCH_ITEMS, payload: str });

export const fetchCollectionSuccessAction = (collection: ICollection) =>
  ({ type: actionTypes.FETCH_ITEMS_SUCCESS, payload: collection })

export const fetchCollectionErrorAction = (error: any) =>
  ({ type: actionTypes.FETCH_ITEMS_ERROR, payload: error })


export const itemSelectAction = (item: IItem): IAction<IItem> =>
  ({ type: actionTypes.ITEM_SELECT, payload: item });

export const showMoreItemsAction = (): IAction<void> =>
  ({ type: actionTypes.SHOW_MORE_ITEMS });
