import { reducer as formReducer } from 'redux-form';
import headerReducer from './header/header-reducer';
import itemsReducer from './items/items-reducer';
import { combineReducers } from 'redux';

const reducers = {
  form: formReducer,
  header: headerReducer,
  items: itemsReducer
}

export default combineReducers(reducers);
