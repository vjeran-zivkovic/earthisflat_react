import React from "react";
import { IntlProvider } from "react-intl";
import { connect } from "react-redux";
import { IAppState } from "../store/app-state";
import en from './common-en.json';
import hr from './common-hr.json';
import { LangEnum } from "../store/header/header-reducer";

interface IProps {
  children: JSX.Element;
  lang: LangEnum;
}

class IntlGlobalProvider extends React.Component<IProps> {
  render() {
    const messages = this.props.lang === LangEnum.HR ? hr : en;
    return (
      <IntlProvider key={this.props.lang} locale={this.props.lang} messages={messages} >
        {this.props.children}
      </IntlProvider>
    )
  }
}

function mapStateToProps(state: IAppState) {
  return {
    lang: state.header.locale.lang
  }
}

export default connect(mapStateToProps)(IntlGlobalProvider)
